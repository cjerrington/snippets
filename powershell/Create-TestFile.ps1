function Create-TestFile {

  [CmdletBinding()]

    param(
    # Default the current directory as the start path
    $filePath = [IO.Directory]::GetCurrentDirectory(),

    # Default a random file name
    $fileName = [System.IO.Path]::GetRandomFileName(),

    # Default is 1MB
    [int64]$fileSize = 1MB
  )

  # Build file path
  $fullPath = Join-Path -Path $filePath -ChildPath $fileName

  Write-Verbose "File Path: $($filePath)"
  write-Verbose "File Name: $($fileName)"
  Write-Verbose "File Size: $($fileSize)"
  Write-Verbose "File Full Path: $($fullPath)"
  
  # Create temp file
  try{
	$tempFile = new-object System.IO.FileStream "$fullPath", Create, ReadWrite
	$tempFile.SetLength($fileSize)
	$tempFile.Close()
	
	Write-Output "Created the following temp file:"
	Get-ChildItem $fullpath | Select-Object FullName,DirectoryName,Name,Length,CreationTime
  }catch{
	  $_
  }
}

Create-TestFile -fileSize 1000MB