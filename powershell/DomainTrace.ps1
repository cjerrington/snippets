[CmdletBinding()]

param(
    # Default domain to use
    [parameter(Position=1)]
    $domain = "google.com"
)

$config = Get-Content -Path secrets.json | ConvertFrom-Json

function Test-PrivateIP {
    <#
        .SYNOPSIS
            Use to determine if a given IP address is within the IPv4 private address space ranges.

        .DESCRIPTION
            Returns $true or $false for a given IP address string depending on whether or not is is within the private IP address ranges.

        .PARAMETER IP
            The IP address to test.

        .EXAMPLE
            Test-PrivateIP -IP 172.16.1.2

        .EXAMPLE
            '10.1.2.3' | Test-PrivateIP
    #>
    param(
        [parameter(Mandatory,ValueFromPipeline)]
        [string]
        $IP
    )
    process {

        if ($IP -Match '(^0.0.0.0)|(^127\.)|(^192\.168\.)|(^10\.)|(^172\.1[6-9]\.)|(^172\.2[0-9]\.)|(^172\.3[0-1]\.)') {
            $true
        }
        else {
            $false
        }
    }    
}

$trace = Test-NetConnection -ComputerName $domain -TraceRoute | Select-Object TraceRoute

$starting = (Invoke-WebRequest "ipinfo.io/?token=$($config.token)").Content | ConvertFrom-Json
"# Running Trace for $domain" | Show-Markdown
Write-Host "Starting from: $($starting.ip)"
Write-Host "$($starting.ip) : $($starting.city), $($starting.region)"

$trace.TraceRoute | ForEach-Object {
    
    if((Test-PrivateIP -IP $_) -eq $False){
        
        $location = (Invoke-WebRequest "ipinfo.io/$($_)?token=19c0fc968d30fe").Content | ConvertFrom-Json
        Write-Host "$_ : $($location.city), $($location.region)"

    }
}

Write-Host ""
$siteHeaders = (Invoke-WebRequest $domain).Headers
"# Domain Details:" | Show-Markdown
$recordA = Resolve-DnsName $domain -Type A
$recordCNAME = Resolve-DnsName $domain -Type CNAME
$recordMX = Resolve-DnsName $domain -Type MX

Write-Host "Server: $($siteHeaders.Server)"
$recordA.IPAddress | ForEach-Object {
    Write-Host "A : $_"
}

if($recordCNAME.NameHost){
    $recordCNAME.NameHost | ForEach-Object {
        Write-Host "CNAME : $_"
    }
}

if($recordMX.NameExchange){
    $recordMX | ForEach-Object {
        Write-Host "MX : $($_.NameExchange):$($_.Preference)"
    }
}
