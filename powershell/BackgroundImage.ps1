#https://github.com/utkarshgpta/bing-desktop-wallpaper-changer/tree/master
#https://stackoverflow.com/questions/10639914/is-there-a-way-to-get-bings-photo-of-the-day
#irm "bing.com$((irm "bing.com/HPImageArchive.aspx?format=js&mkt=en-IN&n=1").images[0].url)" -OutFile bing.jpg
#https://learn.microsoft.com/en-us/previous-versions/bing/search/dd251064(v=msdn.10)?redirectedfrom=MSDN

$uri = "https://www.bing.com/HPImageArchive.aspx?format=js&idx=0&n=1&mkt=en-US"
#Invoke-WebRequest -UseBasicParsing -Uri $uri | Select-Object -ExpandProperty Content | Set-Content -Path "sunmap.jpg" -AsByteStream -Force

$ImageJson = Invoke-WebRequest -Uri $uri -UseBasicParsing | Select-Object -ExpandProperty Content | ConvertFrom-Json
$img = $($ImageJson.images | Select-Object url).url
$img

$FullURL = "https://www.bing.com/$($img)" 

$date = $(Get-Date).ToString("yyyy-MM-dd")

Invoke-WebRequest -Uri $FullURL -UseBasicParsing | Select-Object -ExpandProperty Content | Set-Content -Path "bing-$($date).jpg" -AsByteStream -Force

