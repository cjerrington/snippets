function cht{

    [CmdletBinding()]
    param (
        [Parameter(Mandatory)]
        [string]$cht
    )

    try {
        (Invoke-WebRequest -UseBasicParsing https://cht.sh/$cht).Content
    }
    catch {
        Write-Output "There was an issue finding help."
    }

}

cht :intro