# A simple bookmark system for powershell to set shortcuts to quickly change directories while using the prompt. 
# Created by: Clayton Errington, https://claytonerrington.com
# Version: 1.0 4/19/2023

function Check-Bookmark {
    [CmdletBinding()]
    param(
        $bookmarkfile = "$ENV:USERPROFILE/bookmarks"
    )

    if ( -not [System.IO.File]::Exists($bookmarkfile)){
        New-Item -Path $ENV:USERPROFILE -Name "bookmarks" -ItemType "file" -Value ""
        return $true
    }

    return $true
}

function Add-Bookmark {
    [CmdletBinding()]
    param(
        [Parameter(Position=0,mandatory=$true)]
        [string]$bookmarkName,
        $bookmarkfile = "$ENV:USERPROFILE/bookmarks",
        $bookmarkLocation = $(Get-Location).Path
    )

    $check = Select-String -Path $bookmarkfile -Pattern "^$bookmarkName "
    if ( -not $check.Matches ){
        Add-Content -Path $bookmarkfile -Value "$bookmarkName $bookmarkLocation"
        Write-Output "The bookmark, $bookmarkName, has been added."
    }else {
        Write-Output "The bookmark, $bookmarkName, already exists:"
        $check
    }    
}

function View-Bookmark {
    [CmdletBinding()]
    param(
        $bookmarkfile = "$ENV:USERPROFILE/bookmarks"
    )
    
    $bookmarks = Get-Content -Path $bookmarkfile
    Write-Output "You have $($bookmarks.Count) Bookmarks"
    $bookmarks | ForEach-Object {
        $item = $_ -split " "
        [PSCustomObject]@{
            Bookmark = $item[0]
            Path = $item[1]
        }
    }
}

function Delete-Bookmark {
    [CmdletBinding()]
    param(
        [Parameter(Position=0,mandatory=$true)]
        [string]$bookmarkName,
        $bookmarkfile = "$ENV:USERPROFILE/bookmarks"
    )

    $content = Select-String -Path $bookmarkfile -Pattern "^$bookmarkName "
    if ($content.LineNumber -gt 0){
        $Line = 0;
        [System.IO.File]::ReadLines((Convert-Path $bookmarkfile)).Where({
            $Line++
            $Line -notin $content.LineNumber
        }) | Set-Content $bookmarkfile
        Write-Output "Bookmark removed: $bookmarkName"
    }else{
        Write-Output "Bookmark not found: $bookmarkName"
    }    
}

function Goto-Bookmark {
    [CmdletBinding()]
    param (
        [Parameter(Position=0,mandatory=$true)]
        [string]$bookmarkName,
        $bookmarkfile = "$ENV:USERPROFILE/bookmarks"
    )

    $check = Select-String -Path $bookmarkfile -Pattern "^$bookmarkName "
    if ( -not $check.Matches ){
        Write-Output "The bookmark, $bookmarkName, does not exist"
        View-Bookmark
    }else {
        $path = $check.Line -split " "
        Set-Location -Path $path[1]
    }
    
}

Check-Bookmark | Out-Null
Set-Alias -Name bm -Value Goto-Bookmark -Description "Quickly Set-Location to the bookmarks defined by the user"
Set-Alias -Name bma -Value Add-Bookmark -Description "Add a bookmark to the bookmarks file"
Set-Alias -Name bmd -Value Delete-Bookmark -Description "Delete a bookmark from the bookmarks file"
Set-Alias -Name bmv -Value View-Bookmark -Description "view the bookmarks the user defined"