# Bookmarks for PowerShell

I work in the terminal quite a bit, and needed a quick way to change directories between projects. I started this based off a similar project in bash called [lazy-cd](https://github.com/pedramamini/lazy-cd/blob/master/lcd.sh). This works similarly as it stores the bookmarks you add to a file, and if the bookmark is found, the terminal location will be set accordingly.

## Functions

- `Check-Bookmark` - Initializing function to create bookmark file if needed
- `View-Bookmark` - List all the bookmarks that have been added to the library
- `Add-Bookmark <name>` - Adds the bookmark using the `<name>` and the current location you are in
- `Delete-Bookmark <name>` - Remove the specified bookmark line based on the `<name>`
- `Goto-Bookmark <name>` - If the bookmark `<name>` exists in the bookmark file, the terminal will `Set-Location` to the path defined. 

## Aliases

|Alias|Function|
|-----------|-----------|
| bm | Goto-Bookmark |
| bma | Add-Bookmark |
| bmd | Delete-Bookmark |
| bmv | View-Bookmark |

## Usage

Downlaod the the `bookmark.ps1` file and dot source it to your `Microsoft.PowerShell_profile.ps1`.

```powershell
. "path\to\bookmarks.ps1"

PS C:> bmv
You have 4 Bookmarks

Bookmark      Path
--------      ----
home          C:\Users\username
website       C:\git\cjerrington.github.io
snippets      C:\git\snippets
root          C:\
```
