# https://4sysops.com/archives/encrypt-and-decrypt-files-with-powershell-and-pgp/
# https://www.digitalocean.com/community/tutorials/how-to-use-gpg-to-encrypt-and-sign-messages

# Encrypt with gpg key
$email = "me@claytonerrington.com"

$files = Get-ChildItem "*.*"

$files | ForEach-Object{
    Write-Host $_.FullName
    gpg --encrypt --sign --armor -r $email $_.FullName
}

# Password encrypt
# gpg --batch --passphrase $Password -c $_.FullName