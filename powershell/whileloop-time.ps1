#--- Get RAS Account Credentials
$Domain = "tylerhost.net"
$Hostname = Hostname
#$Username
#$Password
#$Credential

$min = Get-Date '00:00'
$max = Get-Date '05:00'

function runRASIdleCheck {
    write-host $Domain
    write-host $Hostname
}

while($true){
    $now = Get-Date

    if($now.Hour -eq 23){
        # run if hour is 11 PM
        runRASIdleCheck
    }
    elseif ($min.TimeOfDay -le $now.TimeOfDay -and $max.TimeOfDay -ge $now.TimeOfDay) {
        # Run between midnight and 5 AM.
        runRASIdleCheck
    }else {
        write-host "Outside of timeframe to run"
        break
    }

    # Process sleep before running process again...
    Start-Sleep 5

} 

