# Sample.md

This is a simple exmple of how to use and see [markdown](https://google.com). 

## using markdown in the console

This is an exmple of how to render the markdown in a console: `Show-Markdown -Path sample.md`

## using markdown to render an HTML page

This is an exmple of how to render the markdown in a console: `Show-Markdown -Path sample.md -UseBrowser`

# Markdown samples

Using `*` will be a *different color* and bolded content in the console, but italicize the text in the browser.

Using  `__` will bold __content__ in the console.

- list item 1
- list item 2
- list item 3

> Do Block quotes work? 