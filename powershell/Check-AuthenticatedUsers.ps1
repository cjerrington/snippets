$clients = Get-ChildItem -Path D:\test -Directory

$clients | ForEach-Object {
    $_.FullName
    Get-Acl -Path $_.FullName | Select-Object -ExpandProperty Access | Where-Object identityreference -eq "NT AUTHORITY\Authenticated Users"
}