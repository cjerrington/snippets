# https://claytonerrington.com/blog/import-minimum-powershell-module-version/

# Define the minimum required version of your module
$ImportModules = @{
    "AWSPowerShell" = "4.1.602"
    "dbatools" = "2.1.19"
}

# Function to check if the installed module meets the required version
function Get-MyModuleVersion {
    try {
        $module = Get-Module -Name $moduleName -ListAvailable
        if ($module -and ($module.Version -ge [version]$requiredVersion)) {
            # If there happens to be more than one version, select the highest. If only one, that is selected.
            $version = $module.Version | Sort-Object -Descending | Select-Object -First 1
            Write-Host "$moduleName is already at or above ($version) the required version ($requiredVersion)."
            # Reset the required version to the module version using a global variable 
            $global:requiredVersion = $version
            return $true
        } else {
            Write-Host "$moduleName is ($($module.Version)) below the required version ($requiredVersion)."
            return $false
        }
    } catch {
        Write-Host "$moduleName is not installed."
        return $false
    }
}

# Function to install or update the module
function Install-MyModule {
    try {
        [Net.ServicePointManager]::SecurityProtocol = [Net.SecurityProtocolType]::Tls12
        Install-PackageProvider -Name "NuGet" -MinimumVersion "2.8.5.208" -Force -ErrorAction Stop
        Install-Module -Name $moduleName -RequiredVersion $requiredVersion -Force -AllowClobber -SkipPublisherCheck 
        Write-Host "$moduleName module updated to version $requiredVersion."
        return $true
    } catch {
        Write-Host "Failed to update $moduleName module."
        return $false
    }
}

# Import logic
$ImportModules.GetEnumerator() | ForEach-Object {
    Write-Host "Looking for: $($_.Key) with version $($_.Value)"
    $moduleName = $($_.Key)
    $requiredVersion = $($_.Value)

    if (-not (Get-MyModuleVersion)) {

        if (-not (Install-MyModule)) {
            throw "Failed to install or update $moduleName module to version $requiredVersion."
        }
    }

    try{
        # if module is already loaded, unload it so we can import the new version.
        Remove-Module -Name $moduleName -ErrorAction SilentlyContinue
    }catch{}

    Write-Host "Module Name: $moduleName"
    Write-Host "Module version: $requiredVersion"

    Import-Module -Name $moduleName -RequiredVersion $requiredVersion -ErrorAction Stop
}

# Main Logic 
Write-Host "Modules are loaded, begin process..."