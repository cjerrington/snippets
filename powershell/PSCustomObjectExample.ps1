$computers = @($env:COMPUTERNAME, "localhost")

$computers | ForEach-Object {

    $ip=[System.Net.Dns]::GetHostAddresses($_) | Where-Object {$_.AddressFamily -notlike "InterNetworkV6"} | ForEach-Object {
        [PSCustomObject]@{
            IPAddress = $_.IPAddressToString
        }
    }        
    $ProcessCountInfo = Get-CimInstance -ComputerName $_ -ClassName Win32_processor | Select-Object @{'Name' = 'Model'; Expression= {$_.Name}},@{'Name' = 'Description'; Expression= {$_.Caption}},@{'Name' = 'Cores'; Expression= {$_.NumberOfCores}}
    $GetSystemInfo = Get-CimInstance -ComputerName $_ -Class win32_ComputerSystem #| Select-Object DomainRole,Domain,PartOfDomain,Name,TotalPhysicalMemory,Model,Manufacturer
    $Ram = [Math]::Round($GetSystemInfo.TotalPhysicalMemory / 1GB)
    $SerialNumber = Get-CimInstance -ComputerName $_ -Class Win32_SystemEnclosure | Select-Object SerialNumber
    $OS = (Get-CimInstance -ComputerName $_ Win32_OperatingSystem).Caption

    [PSCustomObject]@{
        Hostname = $_
        IPAddress = $ip.IPAddress
        Processor = $ProcessCountInfo.Model
        Cores = $ProcessCountInfo.Cores
        Ram = "$Ram GB"
        OperatingSystem = $OS
        SerialNumber = $SerialNumber.SerialNumber
    }

}