<# The following code returns the IPv4 address of a given alias or host: 
$ips = [System.Net.Dns]::GetHostAddresses('claytonerrington.com').IPAddressToString
$ips

# The below code returns the HostName (CName) and aliases of an IP: 
[System.Net.Dns]::GetHostByAddress('76.76.21.9')

$name = 'claytonerrington.com'
$fqdn = [System.Net.Dns]::GetHostEntry($name).HostName 
$ip = [System.Net.Dns]::GetHostAddresses($fqdn).IPAddressToString
$result = [System.Net.Dns]::GetHostByAddress($ip) 

$fqdn
$ip
$result
#>

[Net.ServicePointManager]::SecurityProtocol = [Net.SecurityProtocolType]::Tls12

Install-Module -Name DnsClient-PS -Scope CurrentUser
Import-Module DNSClient-PS


function Get-DNSRecord {
    [CmdletBinding()]
    Param(
        [Parameter(Position=0, Mandatory=$true, ValueFromPipeline=$true)]
        [String[]]$Name,

        [Parameter(Position=1)]
        [String]$Server
    )

    Begin
    {
        $params = @{
            ErrorAction   = 'Stop'
            ErrorVariable = 'err'
        }
        If ($Server) {
            $params += @{
                 NameServer = $Server
            }
        }
    }

    Process
    {
        foreach($nm in $name)
        {
        
            'SOA', 'A', 'CNAME', 'MX', 'TXT', 'NS' | Foreach-Object {
                $type = $_
                try
                {
                    Resolve-Dns -Query $nm -QueryType $type @params | select -expand answers
                }
                catch
                {
                    write-warning $_.exception.message
                }
            } 
        }
    }
}

Get-DNSRecord www.claytonerrington.com 9.9.9.9