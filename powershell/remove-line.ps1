$filepath = "\test.txt"

$content = Select-String -Path $filepath -Pattern "test"

$Line = 0;
[System.IO.File]::ReadLines((Convert-Path $filepath)).Where({
  $Line++
  $Line -notin $content.LineNumber
}) | Set-Content $filepath
