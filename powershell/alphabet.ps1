Add-Type -AssemblyName System.Speech
$Speech = New-Object System.Speech.Synthesis.SpeechSynthesizer
$Alphabet = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ'.ToCharArray() -as [string[]]

while($true) {
    $Letter = $Alphabet[(Get-Random -Minimum 0 -Maximum $Alphabet.Count)]
    $Assignment = "Press the letter, $Letter"
    $Speech.Speak($Assignment)
    $Answer = Read-Host -Prompt $Assignment
    if ($Answer.Length -gt 0) {
        if ($Letter -eq $Answer) {
            $Speech.Speak("Correct! You pressed the letter, $Answer.")
        } elseif ($Answer -in $Alphabet) {
            $Speech.Speak("Incorrect. You should have pressed, $Letter, but you pressed, $Answer.")
        } else {
            $Speech.Speak("That was not a letter.")
        }
    } else {
        $Speech.Speak("You did not press anything.")
    }
}