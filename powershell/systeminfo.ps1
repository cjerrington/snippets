Add-Type -AssemblyName PresentationFramework

$Disks = get-volume -DriveLetter C | Select-Object DriveLetter,Size,SizeRemaining
$RamInfo = (Get-ComputerInfo | Select-Object -Property OsTotalVisibleMemorySize).OSTotalVisibleMemorySize /1024/1024
$Ram = [Math]::Ceiling($RamInfo)
$CPUCores = (Get-ComputerInfo -Property CsProcessors).CsProcessors | Select NumberOfLogicalProcessors

#where is the XAML file?
$xamlFile = "systeminfo-gui.xaml"

#create window
$inputXML = Get-Content $xamlFile -Raw
$inputXML = $inputXML -replace 'mc:Ignorable="d"', '' -replace "x:N", 'N' -replace '^<Win.*', '<Window'
[xml]$XAML = $inputXML
#Read XAML

$reader = (New-Object System.Xml.XmlNodeReader $xaml)
try {
    $window = [Windows.Markup.XamlReader]::Load( $reader )
}
catch {
    Write-Warning $_.Exception
    throw
}

#Create variables based on form control names.
#Variable will be named as 'var_<control name>'

$xaml.SelectNodes("//*[@Name]") | ForEach-Object {
    #"trying item $($_.Name)";
    try {
        Set-Variable -Name "var_$($_.Name)" -Value $window.FindName($_.Name) -ErrorAction Stop
    } catch {
        throw
   }
}

Get-Variable var_*

$var_lbl_Hostname.Content = $env:COMPUTERNAME
$var_lbl_Ram.Content = "$Ram GB"
$Var_lbl_CPUCores.Content = $CPUCores.NumberOfLogicalProcessors


foreach ($item in $Disks) {
    $var_DiskInfo.Text = $var_DiskInfo.Text  + "Drive Letter: $($item.DriveLetter)`n"
    $var_DiskInfo.Text = $var_DiskInfo.Text  + "Size: $($item.Size)`n"
    $var_DiskInfo.Text = $var_DiskInfo.Text  + "Size Remaining: $($item.SizeRemaining)`n"
}

$Null = $window.ShowDialog()