# Simple function

Function Simple-Function{
    Write-Host "From inside the Simple-Function"
}

# Simple function with parameters
 Function List-Files{
    [CmdletBinding()]
    param (
        [Parameter(Mandatory=$false)]
        [string]$Directory = (Get-Location).Path
    )

   $files = Get-ChildItem -Path $Directory -File

   Write-Host "Directory: $Directory"

   $files | ForEach-Object {
    [pscustomobject]@{
        FileName = $_.Name
        Extension = $_.Extension
        Size = $_.Length
    }
   }
 }

Simple-Function
List-Files 
List-Files -Directory $env:USERPROFILE

$params = @{
    Directory = $env:USERPROFILE
}

List-Files @params