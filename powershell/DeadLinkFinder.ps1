﻿$uri = "https://claytonerrington.com"

$allLinks = [Collections.Generic.HashSet[string]]::new()
$deadLinks = [Collections.Generic.HashSet[string]]::new()

function Add-Link {
    $res = Invoke-WebRequest -UseBasicParsing -Uri $uri
    $links = $res.links.href | Sort-Object | Get-Unique
    $links | ForEach-Object {
        [void]$allLinks.Add($PSItem)
    }
}

Add-Link

<#
function TestLink {
try
{
    $testLink = "$uri$PSItem"
    $testLink
    $Response = Invoke-WebRequest -UseBasicParsing -Uri $testLink
    # This will only execute if the Invoke-WebRequest is successful.
    $StatusCode = $Response.StatusCode
} catch {
    $StatusCode = $_.Exception.Response.StatusCode.value__
}
$StatusCode
}
#>
$allLinks | ForEach-Object {
    if($PSItem.StartsWith('https://')){
        [void]$allLinks.Add($PSItem)
    }elseif ($PSItem.StartsWith('/')) {
        $testLink = $uri + $PSItem
        [void]$allLinks.Add($testLink)
    }else {
        Write-Host $PSItem
    }
}

Write-Host $allLinks