﻿[CmdletBinding()]
param (
    [Parameter(Mandatory=$false)]
    [string]$cwd = (Get-Location).Path
)

Function Git-Status {

    Write-Host "Checking Path: $cwd"

    Get-ChildItem -Path $cwd -Recurse -Depth 2 -Force |
        Where-Object { $_.Mode -match "h" -and $_.FullName -like "*\.git" } |
            ForEach-Object {
                $dir = Get-Item (Join-Path $_.FullName "../")
                pushd $dir
                # TODO: do something with the Git repo here
                $repoStatus = ""
            
                $branchName = git rev-parse --abbrev-ref HEAD
                $repoName = (git remote get-url origin | Split-Path -leaf) -replace ".{4}$"
            
                # Check local status
                $untrackedFiles = git status | Select-String Untracked
                $newFiles = git status | Select-String "new file"
                $modifiedFiles = git status | Select-String modified 
            
                if( $untrackedFiles ){ $repoStatus += "?" }
                if( $newFiles ){ $repoStatus += "+" }
                if( $modifiedFiles ){ $repoStatus += "M" }

                # Check remote status
                $repoAhead=$(git rev-list --count origin/$branchName..$branchName)
                $repoBehind=$(git rev-list --count $branchName..origin/$branchName)

            
                if ( $repoAhead -ne 0 ){ $repoStatus += "^" }
                if ( $repoBehind -ne 0 ){ $repoStatus += "V" }

                # Add items to custom object
                [pscustomobject]@{
                    RepoName = $repoName
                    BranchName = $branchName
                    RepoStatus = $repoStatus
                }
                popd
            }
}

$Object = Git-Status
$Object