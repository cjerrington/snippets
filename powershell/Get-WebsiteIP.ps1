function Get-WebsiteIP {
    [CmdletBinding()]
    param(
        $site = $null
    )
    
    $query = (Resolve-DnsName -Name $site -Type A).IP4Address

    [pscustomobject]@{
        Site 	    = $site
        Type 	    = "A" 
        IP4Address  = $query
    }

}

$websites = @("claytonerrington.com",
              "google.com",
              "vercel.com",
              "yahoo.com")

# Get published users groups
$websites | ForEach-Object {
    Get-WebsiteIP -site $_
}
