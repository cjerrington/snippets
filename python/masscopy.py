import os
from threading import Thread
from time import perf_counter
import shutil

def masscopy(src, dest, ignore=None):
    #print(f"Working on: {src} to {dest}")
    if os.path.isdir(src):
        if not os.path.isdir(dest):
            #print(dest)
            os.makedirs(dest)
        files = os.listdir(src)
        if ignore is not None:
            ignored = ignore(src, files)
        else:
            ignored = set()
        for f in files:
            if f not in ignored:
                masscopy(os.path.join(src, f), os.path.join(dest, f), ignore)

    elif not os.path.exists(dest):
        print(f"{src}")
        shutil.copy2(src, dest)

    elif os.stat(src).st_mtime - os.stat(dest).st_mtime > 1:
        # If more than 1 second difference
        print(f"{src}")
        shutil.copy2(src, dest)
    else:
        pass


def main(src, dest, ignore):
    #path = os.getcwd()
    subfolders = [f.path for f in os.scandir(src) if f.is_dir()]

    # create threads

    threads = list()
    for folder in subfolders:
        x = Thread(target=masscopy, args=(folder, os.path.join(dest, os.path.basename(folder)), ignore,))
        threads.append(x)
        x.start()

    for index, thread in enumerate(threads):
        thread.join()
        
'''
    threads = [Thread(target=masscopy, args=(os.path.join(src, folder), os.path.join(dest, folder), ignore,))
        for folder in subfolders]

    # start the threads
    for thread in threads:
        thread.start()

    # wait for the threads to complete
    for thread in threads:
        thread.join()
        '''
        

if __name__ == "__main__":
    import argparse
    parser = argparse.ArgumentParser(description="Simple File Backup Script")
    parser.add_argument("-s", "--source", help="Source directory")
    parser.add_argument("-d", "--destination", help="destination directory")

    args = parser.parse_args()
    src = args.source
    dest = args.destination

    start_time = perf_counter()

    if src and dest:
        #check if destination is created, if not create it
        if os.path.isdir(src):
            if not os.path.isdir(dest):
                try:
                    os.makedirs(dest)
                except FileExistsError:
                    pass
         
        main(src, dest, ignore=None)
    else:
        parser.print_help()
    
    #main()

    end_time = perf_counter()
    print(f'It took {end_time- start_time :0.2f} second(s) to complete.')
    
