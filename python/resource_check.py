from tqdm import tqdm
from time import sleep
import psutil

# function returning time in hh:mm:ss
def convertTime(seconds):
    minutes, seconds = divmod(seconds, 60)
    hours, minutes = divmod(minutes, 60)
    return "%d:%02d:%02d" % (hours, minutes, seconds)
  
# returns a tuple
battery = psutil.sensors_battery()

# print("Battery percentage : ", battery.percent)
# print("Power plugged in : ", battery.power_plugged)

# # converting seconds to hh:mm:ss
# print("Battery left : ", convertTime(battery.secsleft))

with tqdm(total=100, desc='cpu%', position=1) as cpubar, tqdm(total=100, desc='ram%', position=0) as rambar, tqdm(total=100, desc='bat%', position=2) as bat:
    while True:
        rambar.n=psutil.virtual_memory().percent
        cpubar.n=psutil.cpu_percent()
        bat.n=battery.percent
        rambar.refresh()
        cpubar.refresh()
        bat.refresh()
        sleep(.5)