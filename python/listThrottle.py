import time

emails = [
        "another.user1@domain.com",
        "another.user2@domain.com",
        "another.user3@domain.com",
        "another.user4@domain.com",
        "another.user5@domain.com",
        "another.user6@domain.com",
        "another.user7@domain.com",
        "another.user8@domain.com",
        "another.user9@domain.com",
        "another.user10@domain.com",
        "another.user11@domain.com",
        "another.user12@domain.com",
        "another.user13@domain.com",
        "another.user14@domain.com",
        "another.user15@domain.com",
        "another.user16@domain.com",
        "another.user17@domain.com",
        "another.user18@domain.com",
        "another.user19@domain.com",
        "another.user20@domain.com",
        "another.user21@domain.com",
        "another.user22@domain.com",
        "another.user23@domain.com",
        "another.user24@domain.com",
        "another.user25@domain.com",
    ]

threshold = 3
waitTime = 1

def process_list(emails):
    for email in emails:
        print(email)

for i in range(0, len(emails), threshold):
    print(f"Processing block: {i+1}-{i+threshold}")
    process_list(emails[i:i+threshold])
    time.sleep(waitTime)