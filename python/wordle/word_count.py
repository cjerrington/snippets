from collections import Counter
from datetime import datetime

def percentage(part, whole):
  # calculate the percentage of letters used
  return round(100 * float(part)/float(whole), 2)


def main(wordledate):
  # the main function to process the words used in the wordle by date files
  
  # Define a filename and month given from the arguments
  filename = f"{wordledate}_words.txt"

  # read the words into the script
  with open(filename) as file:
      data = file.read().replace('\n','')

  # start our collection and sum up the total number of each time a letter is used
  collection = Counter(data)
  total = sum(collection.values())

  # calculate the date and update based on the leading 0 of the file/month name
  mydate = datetime.strptime(wordledate, "%m").strftime("%B") 
  print(f"For the month of {mydate}, {len(collection)} letters used of the alphabet")

  # Loop over the items in the collection and calculate the percent
  for letter, count in sorted(collection.items()):
      percent = percentage(count, total)
      print(f"{letter} : {percent}%")


if __name__ == "__main__":
    import argparse
    
    parser = argparse.ArgumentParser(description="Wordle letter parser")
    parser.add_argument("-d", "--date", help="two digit date of the month")

    args = parser.parse_args()
    wordledate = args.date

    # send the requested month to the function, otherwise, default to the current month
    if wordledate:
      main(wordledate)
    else:
      current_month = datetime.now().strftime('%m')
      main(current_month)