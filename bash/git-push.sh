function gitpush(){
    git remote -v
    read -p "Do you want to proceed? (yes/no) " -t 10 yn

    if [ -z "$yn" ]
    then
          echo -e "\nerror: no response detected"
          exit 1
    fi

    case $yn in 
	    y ) echo ok, we will proceed;;
	    n ) echo exiting...;
		    exit;;
	    * ) echo invalid response;
		    exit 1;;
    esac

    echo Here are the changed files
    git add . -n

    read -p "Enter the commit message: " message
    git add .
    git commit -S -m "$message"
    git push origin $(git branch --show-current)
    read -p "Push Completed!"
}