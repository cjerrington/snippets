#!/bin/bash

# Get the current date
DATE=$(date +%Y-%m-%d)

# Get the user's home directory
USER_HOME=$(getent passwd $USER | cut -d: -f6)

# Create a backup directory for the current date, if it doesn't exist
BACKUP_DIR="${USER_HOME}/backup/${DATE}"
mkdir -p "${BACKUP_DIR}"

# Tar and compress the user's profile directory, excluding the excluded folders
tar \
    --exclude="${USER_HOME}/.mozilla" \
    --exclude="${USER_HOME}/.cache" \
    --exclude="${USER_HOME}/.config" \
    --exclude="${USER_HOME}/.cinnamon" \
    --exclude="${USER_HOME}/.local" \
    --exclude="${USER_HOME}/backup*" \
    -czvf "${BACKUP_DIR}/profile.tar.gz" "${USER_HOME}" 

# Delete old backups that are more than 2 weeks old
find "${USER_HOME}/backup/" -type d -mtime +14 -exec rm -rf {} \;
