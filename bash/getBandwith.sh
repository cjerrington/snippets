total_bytes_received=$(cat /proc/net/dev | grep -v "lo:" | awk '{print $2}' | sort -n | tail -1)
total_bytes_uploaded=$(cat /proc/net/dev | grep -v "lo:" | awk '{print $10}' | sort -n | tail -1)

total_kb_received=$((total_bytes_received / 1024))
total_mb_received=$((total_kb_received / 1024))
total_gb_received=$((total_mb_received / 1024))

total_kb_uploaded=$((total_bytes_uploaded / 1024))
total_mb_uploaded=$((total_kb_uploaded / 1024))
total_gb_uploaded=$((total_mb_uploaded / 1024))

echo "Total received since last boot: $total_mb_received MB / $total_gb_received GB"
echo "Total uploaded since last boot: $total_mb_uploaded MB / $total_gb_uploaded GB"