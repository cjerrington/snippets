@echo off
cls

REM show the remote location branch
echo Does this look correct?
echo.
git remote -v

echo.
REM show the changed fils
echo Here are the changed files
git add . -n

echo.
REM set a commit message if all is good
set /p message="Enter commit Message: "
echo Here is your message: %message%

REM add files and commit and push
git add .
git commit -m "%message%"
git push origin master
echo Update complete! 