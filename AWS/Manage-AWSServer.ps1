<#
#   Created by: Clayton Errington
#   Purpose: Manage AWS Server Instance states and perform the following actions on an EC2 instance: 
#       - Start-AWSServer
#       - Stop-AWSInstance
#       - Restart-AWSInstance
#       - Get-AWSInstanceID
#       - Get-AWSInstanceName
#>
function Select-AWSProfile {
    $getProfiles = Get-Content $env:USERPROFILE\.aws\config | select-string -pattern "profile*"

    $profiles = $getProfiles.line.Replace("[profile ","").Replace("]","")
    
    $menu = @()
    
    foreach ($profile in $profiles){
        $menu += $profile
    }
    $menu += "Not listed"
    
    $i = 1
    foreach ($item in $menu){
        write-host "$i - $item"
        $i++
    }
    
    $choice = Read-Host "Pick your profile"
    
    # Since array starts at 0, lets humanize the choice
    $choice = $choice-1
    
    if ($menu[$choice] -eq "Not listed" ) {
        aws configure sso
        Select-AWSProfile
    }elseif($menu -contains $menu[$choice]){ 
        $chosenProfile = $menu[$choice]
        return $chosenProfile
    }else{
        Select-AWSProfile
    }
}

function Test-SessionActive {
    [CmdletBinding()]
    param (
        $chosenProfile = "default"
    )
    
    # Check the profile session is active
    Write-Host "Checking profile: $chosenProfile"
    $sessionActive = (aws sts get-caller-identity --query "Account" --profile $chosenProfile | Select-Object Length).Length

    if ($sessionActive -eq 14){
        #Session still active
        return $True
    }else{
        # We have a profile so try to login
        aws sso login --profile $chosenProfile
        #return $False
    }
}

function Get-AWSInstanceID {
    [CmdletBinding()]
    param(
        $ServerName = $null,
        $chosenProfile = $null,
        $region = "us-east-1",
        [switch]$copy
    )

    if ($null -eq $chosenProfile) {
        $chosenProfile = Select-AWSProfile
    }

    if (Test-SessionActive -chosenProfile $chosenProfile){
        #Session still active
        if($null -eq $ServerName){
            $ServerName = Read-Host -Prompt "What is the servename"
        }
        Write-Host "Attempting to find Instance ID for $ServerName in $region"
        $instanceID = (aws ec2 describe-instances --filters "Name=tag:Name,Values=$($ServerName)" --query "Reservations[].Instances[].InstanceId" --profile $chosenProfile --output text --region $region)
        if($copy){
            $instanceID | clip
        }
        return $instanceID
    }else{
        # Error in the process
        Write-Host "There was an issue running the process: $($MyInvocation.MyCommand)"
    }
}

function Get-AWSInstanceName {
    [CmdletBinding()]
    param(
        $instanceID = $null,
        $chosenProfile = $null,
        $region = "us-east-1",
        [switch]$copy
    )

    if ($null -eq $chosenProfile) {
        $chosenProfile = Select-AWSProfile
    }

    if (Test-SessionActive -chosenProfile $chosenProfile){
        #Session still active
        if($null -eq $instanceID){
            $instanceID = Read-Host -Prompt "What is the Instance ID?"
        }
        Write-Host "Attempting to find Instance Name for $instanceID in $region"
        $ServerName = (aws ec2 describe-instances --instance-id $instanceID --profile $chosenProfile --query "Reservations[].Instances[].Tags[?Key=='Name'].Value" --region $region --output text)
        if($copy){
            $ServerName | clip
        }
        return $ServerName
    }else{
        # Error in the process
        Write-Host "There was an issue running the process: $($MyInvocation.MyCommand)"
    }
}

function Start-AWSInstance {
    [CmdletBinding()]
    param(
        $ServerName = $null,
        $chosenProfile = $null,
        $region = "us-east-1"
    )

    # Read the profiles and send user to select the one they want
    if ($null -eq $chosenProfile) {
        $chosenProfile = Select-AWSProfile
    }

    if (Test-SessionActive -chosenProfile $chosenProfile){
        #Session still active
        if($null -eq $ServerName){
            $ServerName = Read-Host -Prompt "What is the servename"
        }
        Write-Host "Attempting to start $ServerName in $region"
        $instanceID = (aws ec2 describe-instances --filters "Name=tag:Name,Values=$($ServerName)" --query "Reservations[].Instances[].InstanceId" --profile $chosenProfile --output text --region $region)
        aws ec2 start-instances --instance-ids $instanceID --profile $chosenProfile --region $region
    }else{
        # Error in the process
        Write-Host "There was an issue running the process: $($MyInvocation.MyCommand)"
    }
}

function Stop-AWSInstance {
    [CmdletBinding()]
    param(
        $ServerName = $null,
        $chosenProfile = $chosenProfile,
        $region = "us-east-1"
    )

    # Read the profiles and send user to select the one they want
    if ($null -eq $chosenProfile) {
        $chosenProfile = Select-AWSProfile
    }

    if (Test-SessionActive -chosenProfile $chosenProfile){
        #Session still active
        if($null -eq $ServerName){
            $ServerName = Read-Host -Prompt "What is the servename"
        }
        Write-Host "Attempting to stop $ServerName in $region"
        $instanceID = (aws ec2 describe-instances --filters "Name=tag:Name,Values=$($ServerName)" --query "Reservations[].Instances[].InstanceId" --profile $chosenProfile --region $region --output text)
        aws ec2 stop-instances --instance-ids $instanceID --profile $chosenProfile --region $region
    }else{
        # Error in the process
        Write-Host "There was an issue running the process: $($MyInvocation.MyCommand)"
    }
}

function Restart-AWSInstance {
    [CmdletBinding()]
    param(
        $ServerName = $null,
        $chosenProfile = $chosenProfile,
        $region = "us-east-1"
    )

    # Read the profiles and send user to select the one they want
    if ($null -eq $chosenProfile) {
        $chosenProfile = Select-AWSProfile
    }

    if (Test-SessionActive -chosenProfile $chosenProfile){
        #Session still active
        if($null -eq $ServerName){
            $ServerName = Read-Host -Prompt "What is the servename"
        }
        $instanceID = (aws ec2 describe-instances --filters "Name=tag:Name,Values=$($ServerName)" --query "Reservations[].Instances[].InstanceId" --profile $chosenProfile --region $region --output text)
        Write-Host "Attempting to stop $ServerName in $region"
        aws ec2 stop-instances --instance-ids $instanceID --profile $chosenProfile --region $region
        do {
            # get instance state until it returns Code: 80 as stopped
            $instanceState = (aws ec2 describe-instances --filters "Name=tag:Name,Values=$($ServerName)" --query "Reservations[].Instances[].State[].Code" --profile $chosenProfile --region $region --output text)
            Write-Host "Waiting for server $ServerName to stop..."
            Start-Sleep -Seconds 1
        } until ( $instanceState -eq 80 )
        Write-Host "Attempting to start $ServerName in $region"
        aws ec2 start-instances --instance-ids $instanceID --profile $chosenProfile --region $region
    }else{
        # Error in the process
        Write-Host "There was an issue running the process: $($MyInvocation.MyCommand)"
    }
}